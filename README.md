# Test frontend EdTeam

Run project

1.- Clone this repository
```
git clone ....
```

2.- Install dependencies
```
npm install
```
or

```
yarn
```

2.- Run project in production mode

```
npm start
```


Run project in development mode
```
npm run start:dev
```

3.- Open browser
```
http://localhost:3000
```