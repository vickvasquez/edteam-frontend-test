import superagent from 'superagent';

import config from '../config/config';

const { apiHost } = config;

const request = (data = {}, endpoint, method = 'GET') => {
  const req = superagent(method, apiHost + endpoint);

  const params = data;
  Object.keys(params).forEach(key => params[key] === null && delete params[key]);

  req
    .set('Accept', 'application/json')
    .query({
      no_cache: new Date().getTime(),
    });


  return new Promise((resolve, reject) => {
    req.end((err, res) => {
      if (err) {
        reject(err);
      } else {
        const { body } = res;
        if (body && body.errors) {
          reject(body.errors);
        } else if (body) {
          resolve(body);
        } else if (res.text) {
          resolve({
            text: res.text,
          });
        }
      }
    });
  });
};

const methods = {
  post(endpoint, data) {
    return request(data, endpoint, 'POST');
  },
  get(endpoint, data) {
    return request(data, endpoint);
  },
};

export default methods;
