const apiHost = process.env.API_HOST || window.API_HOST || 'https://jsonplaceholder.typicode.com';

export default {
  title: 'Test Frontend EdTeam',
  apiHost,
};
