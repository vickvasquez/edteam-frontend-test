const path = require('path');

const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  mode: 'development',
  module: {
    rules: [{
      test: /\.(js|jsx)$/,
      exclude: /node_modules/,
      use: [{
        loader: 'babel-loader',
      },
      {
        loader: 'eslint-loader',
      }]
    }],
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: path.resolve('./public/index.html'),
      filename: 'index.html',
    }),
  ],
  devtool: '#source-map',
  resolve: {
    modules: ['node_modules'],
    alias: {
      '~components': path.resolve('./src/components'),
      '~pages': path.resolve('./src/pages'),
    },
  },
  devServer: {
    port: process.env.PORT || 3000,
  },
};
